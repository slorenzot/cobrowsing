var Util = {
    parseHash: function () {
        let hash = window.location.hash.substr(1)
        return hash
            .split('&')
            .reduce(function (result, item) {
                var params = item.split('=');
                result[params[0]] = params[1];
                return result;
            }, {});
    },
    delay: function (ms) {
        return new Promise(function (r) {
            setTimeout(r, ms)
        })
    },
    rebind: function ($el, handler) {
        return $el.unbind().bind('click', handler)
    }
}