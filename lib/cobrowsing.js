const host = "192.168.0.189",
    port = 9000

var Cobrowser = {
    role: 'client',
    debug: false,
    allowCobrowsing: false,
    requestCobrowsingOnLoad: true,
    hideSensitiveInformation: false,
    ownMovement: false,
    socket: null,
    clientId: null,
    remoteId: null,
    initialUrl: "",
    currentUrl: "",
    client: {
        screen: {
            width: 0,
            height: 0
        }
    },
    events: {
        COBROWSE_REQUEST: 'requestcobrowsing',
        COBROWSE_ACCEPT: 'acceptcobrowsing',
        COBROWSE_REJECT: 'rejectedcobrowsing',
        COBROWSE_REQUEST_SCREENSHOT: 'requestscreenshot',
        COBROWSE_ACCEPTS_CREENSHOT: 'acceptscreenshot',
        COBROWSE_URL_CHANGE: 'urlchange',
        COBROWSE_MOUSE_OVER: 'mouseover',
        COBROWSE_MOUSE_MOVE: 'mousemove',
        COBROWSE_MOUSE_OUT: 'mouseout',
        COBROWSE_SELECTION: 'selection',
        COBROWSE_CLICK: 'click',
        COBROWSE_SCROLL: 'scroll',
        COBROWSE_KEYUP: 'keyup',
    },
    generateKey: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        })
    },
    title: function (title) {
        window.top.document.title = title
    },
    cursor: function (visibility) {
        let $pointer = $("#pointer")
        $pointer
            .css({
                visibility: visibility
            })
    },
    toggle: function (isMaster) {
        this.allowCobrowsing = isMaster || !!!this.allowCobrowsing

        if (this.allowCobrowsing) {
            Cobrowser.cursor('visible')

            console.log("switched to allow cobrowsing...")
            Cobrowser.title("CoBrowsing...")
        } else {
            Cobrowser.cursor('hidden')

            console.log("switched to disallow cobrowsing...")
            Cobrowser.title("No cobrowsing...")
        }
    }
    ,
    start: function () {
        this.allowCobrowsing = true;
        this.showPanel();
    }
    ,
    stop: function () {
        this.allowCobrowsing = false;
        this.hidePanel();
    }
    ,
    showPanel: function () {
        $(".cobrowsing_panel")
            .animate({
                top: '0px'
            })
        $(".cobrowsing_pointer")
            .show()
    },
    hidePanel: function() {
        $(".cobrowsing_panel")
        .animate({
            top: '-80px'
        })
        $(".cobrowsing_pointer")
            .hide()
    },
    screenSize: function () {
        let $canvas = $("#screen")
        return {width: $canvas.width(), height: $canvas.height()}
    },
    request: function (remoteId) {
        Cobrowser.startLoadingAnimation("Iniciando, cobrowsing, se espera autorización del cliente...")

        try {
            let requestEvent = {
                origin: {
                    screen: {
                        width: Cobrowser.client.screen.width,
                        height: Cobrowser.client.screen.height
                    }
                },
                role: Cobrowser.role,
                type: 'requestcobrowsing',
                from: Cobrowser.clientId,
                to: remoteId || Cobrowser.remoteId
            }

            Cobrowser.socket.send(JSON.stringify(requestEvent))
        } catch (e) {
            console.log(e)
        }
    },
    accept: function () {
        let _window = $("#screen")[0].contentWindow

        try {
            let acceptEvent = {
                origin: {
                    screen: {
                        width: Cobrowser.client.screen.width,
                        height: Cobrowser.client.screen.height
                    }
                },
                role: Cobrowser.role,
                type: 'acceptcobrowsing',
                from: Cobrowser.clientId,
                to: Cobrowser.remoteId,
                initialUrl: _window.location.href
            }

            Cobrowser.socket.send(JSON.stringify(acceptEvent))
            Cobrowser.showPanel()
        } catch (e) {
            console.log(e)
        }
    },
    reject: function () {
        try {
            let rejectEvent = {
                origin: {
                    screen: {
                        width: Cobrowser.client.screen.width,
                        height: Cobrowser.client.screen.height
                    }
                },
                role: Cobrowser.role,
                type: 'rejectedcobrowsing',
                from: Cobrowser.clientId,
                to: Cobrowser.remoteId
            }

            Cobrowser.socket.send(JSON.stringify(rejectEvent))
        } catch (e) {
            console.log(e)
        }
    },
    attach: function (win, opt, handlers = {
        onAttached: function () {
            // Not implemented
        },
        onDettached: function () {
            // Not implemented
        },
        onStarted: function () {
            // Not implemented
        },
        onStopped: function () {
            // Not implemented
        },
        onAccepted: function () {
            // Not implemented
        },
        onRejected: function () {
            // Not implemented
        }
    }) {
        Cobrowser.clientId = opt.clientId || Cobrowser.generateKey().substr(0, 8)
        Cobrowser.remoteId = opt.remoteId
        Cobrowser.requestCobrowsingOnLoad = opt.requestCobrowsingOnLoad || false

        let $canvas = $("#screen"),
            width = Cobrowser.screenSize().width,
            height = Cobrowser.screenSize().height
        Cobrowser.client.screen.width = width
        Cobrowser.client.screen.height = height
        Cobrowser.debug = true

        let url = $canvas[0].contentDocument.location.href

        console.log(`clientId: ${Cobrowser.clientId}`)
        console.log(`remoteId: ${Cobrowser.remoteId}`)
        console.log(`screen size: ${width}x${height}`)
        console.log(`Initial Url to cobrowsing: ${url}`)

        this.socket = new WebSocket(`ws://${host}:${port}/cobrowsing/socket.php`)
        // this.socket = new WebSocket(`wss://${host}/cobrowsing`)
        this.socket.onopen = function (e) {
            let socket = this;

            console.log("Socket for cobrowsing started...")
            console.log("=============================")

            $canvas
                .on('load', function () {
                    // if (!Cobrowser.allowCobrowsing) return

                    let x, y, $screen = $("#screen")[0]

                    Cobrowser.title("Cargando...")

                    if ($screen == null) return;

                    let _window = $screen.contentWindow,
                        _document = $screen.contentDocument,
                        location = _window.location.href

                    if (Cobrowser.currentUrl == location) return

                    Cobrowser.currentUrl = location
                    Cobrowser.title(_document.title)

                    console.log(`Url changed to: ${location}`)

                    let urlEvent = {
                        origin: {
                            screen: {
                                width: width,
                                height: height
                            }
                        },
                        role: Cobrowser.role,
                        type: 'urlchange',
                        url: _window.location.href,
                        from: Cobrowser.clientId,
                        to: Cobrowser.remoteId
                    }
                    socket.send(JSON.stringify(urlEvent))

                    let tags = $("iframe#screen").contents()
                        .find("*")

                    tags.map(function (i, el) {
                        if (el.id == undefined || el.id == "") $(el).prop('id', `map-${i}`)
                        return e
                    })

                    tags
                        .mouseover(function (e) {
                            if (!Cobrowser.allowCobrowsing) return

                            let id = e.currentTarget.id
                            // if (Cobrowser.allowCobrowsing) return

                            let mouseoverEvent = {
                                origin: {
                                    screen: {
                                        width: width,
                                        height: height
                                    }
                                },
                                role: Cobrowser.role,
                                type: 'mouseover',
                                element: id,
                                from: Cobrowser.clientId,
                                to: Cobrowser.remoteId
                            }

                            if (id != "" && id != undefined) socket.send(JSON.stringify(mouseoverEvent))
                        })

                    let $in = $($("#screen")[0].contentWindow)
                    $in
                        .on('click', function (e, remoteEvent) {
                            if (!Cobrowser.allowCobrowsing) return

                            if (e.srcElement !== undefined) {
                                // Si el evento es enviado remotamente se descarta para eviar loops
                                return
                            }

                            if (remoteEvent) {
                                e.stopImmediatePropagation()
                                e.stopPropagation()

                                return
                            }

                            let id = e.toElement.id,
                                name = e.toElement.name

                            if (e.toElement.id == undefined && e.toElement.name == undefined) {
                                console.log('clicked element has no tag id or name, ignoring click')
                                return
                            }

                            let clickEvent = {
                                origin: {
                                    screen: {
                                        width: width,
                                        height: height
                                    }
                                },
                                role: Cobrowser.role,
                                type: 'click',
                                element: {
                                    type: e.toElement.tagName,
                                    id: e.toElement.id,
                                    name: e.toElement.tagName
                                },
                                x: x,
                                y: y,
                                from: Cobrowser.clientId,
                                to: Cobrowser.remoteId
                            }
                            socket.send(JSON.stringify(clickEvent))
                        })

                    // _window.onclick = function (e, remoteEvent) {
                    //     if (e.srcElement === undefined) return
                    //     console.log(e)
                    //     console.log(remoteEvent)
                    //
                    //     if (remoteEvent) {
                    //         e.stopImmediatePropagation()
                    //         e.stopPropagation()
                    //
                    //         return
                    //     }
                    //
                    //     let id = e.toElement.id,
                    //         name = e.toElement.name
                    //
                    //     let clickEvent = {
                    //         origin: {
                    //             screen: {
                    //                 width: width,
                    //                 height: height
                    //             }
                    //         },
                    //         role: Cobrowser.role,
                    //         type: 'click',
                    //         element: {
                    //             type: e.toElement.tagName,
                    //             id: id,
                    //             name: name
                    //         },
                    //         x: x,
                    //         y: y,
                    //         from: Cobrowser.clientId,
                    //         to: Cobrowser.remoteId
                    //     }
                    //     socket.send(JSON.stringify(clickEvent))
                    // }
                    _window.onmouseover = function (e) {
                        if (!Cobrowser.allowCobrowsing) return

                        let hideSensitive = Cobrowser.hideSensitiveInformation

                        if (hideSensitive) {
                            console.log('Hidding sensitive information, hidding cursor movements....')
                            return
                        }

                        if (e === null || e.fromElement === null) return;

                        let id = e.fromElement.id,
                            name = e.fromElement.name

                        let mouseEvent = {
                            origin: {
                                screen: {
                                    width: width,
                                    height: height
                                }
                            },
                            role: Cobrowser.role,
                            type: 'mouseover',
                            element: {
                                type: e.fromElement.tagName,
                                id: id,
                                name: name
                            },
                            x: x,
                            y: y,
                            from: Cobrowser.clientId,
                            to: Cobrowser.remoteId
                        }

                        // console.log(mouseEvent)

                        socket.send(JSON.stringify(mouseEvent))
                    }
                    _window.onmousemove = function (e) {
                        if (!Cobrowser.allowCobrowsing) return

                        let hideSensitive = Cobrowser.hideSensitiveInformation

                        if (hideSensitive) {
                            console.log('Hidding sensitive information, hidding cursor movements....')
                            return
                        }

                        x = e.clientX
                        y = e.clientY

                        let mouseEvent = {
                            origin: {
                                screen: {
                                    width: width,
                                    height: height
                                }
                            },
                            role: Cobrowser.role,
                            type: 'mousemove',
                            x: x,
                            y: y,
                            from: Cobrowser.clientId,
                            to: Cobrowser.remoteId
                        }
                        socket.send(JSON.stringify(mouseEvent))
                    }
                   _window.onscroll = function () {
                    if (!Cobrowser.allowCobrowsing) return

                        let hideSensitive = Cobrowser.hideSensitiveInformation, 
                            ownMovement = Cobrowser.ownMovement;

                        if (hideSensitive) {
                            console.log('Hidding sensitive information, hidding cursor movements....')
                            return
                        }

                        if (ownMovement) {
                            console.log("Own movement");
                            return;
                        }

                        let x = _window.scrollX || _window.document.scrollX || 0,
                            y = _window.scrollY || _window.document.scrollY || 0
                        let scrollEvent = {
                            origin: {
                                screen: {
                                    width: width,
                                    height: height
                                }
                            },
                            role: Cobrowser.role,
                            type: 'scroll',
                            left: x,
                            top: y,
                            from: Cobrowser.clientId,
                            to: Cobrowser.remoteId
                        }
                        socket.send(JSON.stringify(scrollEvent))
                    }
                    _window.ontouchend = function (e) {
                        if (!Cobrowser.allowCobrowsing) return
                    }
                    _window.onkeyup = function (e) {
                        if (!Cobrowser.allowCobrowsing) return

                        let hideSensitive = Cobrowser.hideSensitiveInformation,
                            source = e.srcElement.id,
                            key = e.key,
                            code = e.code,
                            value = undefined

                        if (hideSensitive) console.log('Hiding sensitive information....')

                        if (key === undefined) {
                            console.log("Multiple value or autofill browser selected")

                            source = e.srcElement.id
                            value = e.srcElement.value
                            key = undefined
                            code = undefined
                        }

                        if (hideSensitive && !(key in [
                            'undefined',
                            'Delete',
                            'Enter',
                            'Meta',
                            'Escape',
                            'Tab',
                            'Shift',
                            'CapsLock',
                            'Alt',
                            'AltGraph',
                            'Control',
                            'ArrowLeft',
                            'ArrowRight',
                            'ArrowUp',
                            'ArrowDown'])) {

                            key = "*"
                            code = 42
                        }

                        let keyEvent = {
                            origin: {
                                screen: {
                                    width: width,
                                    height: height
                                }
                            },
                            role: Cobrowser.role,
                            type: 'keyup',
                            source: source,
                            key: key,
                            code: code,
                            value: value,
                            from: Cobrowser.clientId,
                            to: Cobrowser.remoteId
                        }
                        socket.send(JSON.stringify(keyEvent))
                    }

                    $in.select(function (e) {
                        if (!Cobrowser.allowCobrowsing) return

                        var hideSensitive = Cobrowser.hideSensitiveInformation,
                            start = e.target.selectionStart,
                            end = e.target.selectionEnd,
                            source = e.target.id,
                            selection = _document.getSelection()

                        if (hideSensitive) console.log('Hiding sensitive information....')
                        // console.log(selection.rangeCount)

                        let range = selection.getRangeAt(0)

                        console.log(range)
                        // console.log(selection.selectionEnd)

                        let selectionEvent = {
                            origin: {
                                screen: {
                                    width: width,
                                    height: height
                                }
                            },
                            role: Cobrowser.role,
                            type: Cobrowser.events.COBROWSE_SELECTION,
                            source: source,
                            start: start,
                            end: end,
                            from: Cobrowser.clientId,
                            to: Cobrowser.remoteId
                        }
                        socket.send(JSON.stringify(selectionEvent))
                    })

                    // _document.onselectionchange = function (e) {
                    //     let hideSensitive = Cobrowser.hideSensitiveInformation,
                    //         source = e.srcElement.id,
                    //         selection = _document.getSelection()
                    //
                    //     if (hideSensitive) console.log('Hiding sensitive information....')
                    //     // console.log(selection.rangeCount)
                    //
                    //     let range = selection.getRangeAt(0)
                    //
                    //     console.log(range)
                    //     // console.log(selection.selectionEnd)
                    //
                    //     let selectionEvent = {
                    //         origin: {
                    //             screen: {
                    //                 width: width,
                    //                 height: height
                    //             }
                    //         },
                    //         role: Cobrowser.role,
                    //         type: Cobrowser.events.COBROWSE_SELECTION,
                    //         source: source,
                    //         from: Cobrowser.clientId,
                    //         to: Cobrowser.remoteId
                    //     }
                    //     socket.send(JSON.stringify(selectionEvent))
                    // }

                    console.log('Page loaded calling attached events...')

                    handlers.onAttached(opt.remoteId || Cobrowser.remoteId)

                    console.log("Ready!")
                })
        }
        this.socket.onmessage = function (e) {
            let $screen = $("#screen")
            let $pointer = $("#pointer")

            if ($screen == null) return;
            let _window = $screen[0].contentWindow
            let event = JSON.parse(e.data)

            /**
             * Solicitud de inicio de Cobrowsing
             */
            if (!Cobrowser.allowCobrowsing) {
                if (event.type == 'requestcobrowsing') {
                    if (event.to !== Cobrowser.clientId) {
                        console.log(`Invite CoBrowsing to ${event.to}... Isn't mine, ignoring!`)
                        return;
                    }

                    console.log(event.type + " => " + event.from)

                    Cobrowser.remoteId = event.from
                    
                    if (confirm("Le gustaría iniciar la navegación compartida?\n\n" +
                        "Un gente está solicitando asistirle de forma segura en nuestro sitio web. No podrá ver su pantalla, información delicada o ninguna otra pestaña del navegador.")) {
                        // if (!window.Notification) {
                        //     console.log('Sorry, notifications are not supported.')
                        // } else {
                        //     Notification.requestPermission(function (p) {
                        //         if (p === 'denied') {
                        //             console.log('Notifications denied by user.')
                        //         } else {
                        //             console.log('Notifications allowed by user.')
                        //
                        //             let notification = new Notification('Navegación compartida', {
                        //                 body: 'Le gustaría iniciar la navegación compartida? El agente podrá asistirle de forma segura en nuestro sitio web. No podremos ver su pantalla, información delicada o ninguna otra pestaña del navegador.',
                        //                 icon: ''
                        //             })
                        //         }
                        //     })
                        // }
                        Cobrowser.remoteId = event.from
                        Cobrowser.accept()
                        Cobrowser.start()
                    } else {
                        Cobrowser.reject()
                    }
                }

                return;
            }

            switch (event.type) {
                /**
                 * Notificación de aceptación de invitación de Cobrowsing
                 */
                case 'acceptcobrowsing':
                    if (event.to !== Cobrowser.clientId) return

                    console.log(event.type + " => " + event.url)

                    /**
                     * Si la URl no es la misma en ambos navegadores se
                     * encarga de ubicar al agente en la seccion donde se
                     * encuentra el cliente
                     */
                    if (Cobrowser.currentUrl !== event.initialUrl) {
                        $screen.attr('src', event.initialUrl)
                        _window.scrollTo(0, 0)
                    }
                    let width = event.origin.screen.width,
                        height = event.origin.screen.height

                    console.log(`Cliente resolution: ${width}x${height}`)
                    console.log('Session accepted by the client!')

                    Cobrowser.resize(width, height)
                    Cobrowser.stopLoadingAnimation("Iniciando...")
                    break;

                case 'rejectedcobrowsing':
                    if (event.from !== Cobrowser.remoteId) return;

                    console.log(event.type + " => " + event.url)

                    Cobrowser.stopLoadingAnimation("El cliente ha cancelado la sesión...")

                    console.log('Session rejected by the client!')
                    break;

                /**
                 * Solicitud de captura de pantalla
                 */
                case 'requestscreenshot':
                    if (event.to !== Cobrowser.clientId) return;

                    console.log(event.type + " => " + event.url)

                    // let target = $screen
                    let target = $screen
                        .contents()
                        .find('body')[0]

                    if (confirm("Un agente desea tomar una captura su pantalla.\n\nDesea permitir que el agentes captura la pantalla actual?"))
                        html2canvas(target)
                            .then(function (canvas) {
                                // Export the canvas to its data URI representation
                                let base64image = canvas.toDataURL("image/png");

                                console.log(base64image)

                                let capturaEvent = {
                                    type: 'retrievescreenshot',
                                    title: "Sin titulo",
                                    image: base64image
                                }
                                Cobrowser.socket.send(JSON.stringify(capturaEvent))
                            });
                    break;

                /**
                 * Recepción de captura de pantalla
                 */
                case 'retrievescreenshot':
                    if (event.to !== Cobrowser.clientId) return;
                    // Open the image in a new window
                    // _window.open(base64image, "_blank");

                    $("#cobrowsing_screenshot_canvas").css("background-image", "url('data:image/png;base64," + base64String + "')")
                    $("#cobrowsing_screenshot_modal").modal('show')
                    break;

                /**
                 * Notificación de cambio de URL, necesario para la navegación colaborativa
                 */
                case 'urlchange':
                    if (event.to !== Cobrowser.clientId) return;

                    console.log(event.type + " => " + event.url)

                    if (Cobrowser.currentUrl == event.url) return

                    $screen.attr('src', event.url)
                    _window.scrollTo(0, 0)
                    break;

                /**
                 * Notificación de colocación de puntero del ratón sobre elemento del DOM
                 */
                case 'mouseover':
                    if (event.to !== Cobrowser.clientId) return;
                    let selec = ""
                    if (event.element.id !== undefined && event.element.id !== "") {
                        selec = `#${event.element.id}`
                    } else if (event.element.name !== undefined && event.element.name !== "") {
                        selec = `[name=${event.element.name}]`
                    } else return

                    console.log(event.type + " => " + selec)

                    // if (event.from !== Cobrowser.clientId)
                    let $eleme = $($(selec, $screen.contents())[0])

                    // console.log($eleme[0])
                    // $eleme
                    //     .parent()
                    //     .mouseover(function (e) {
                    //         e.stopPropagation()
                    //     })
                    if ($eleme[0] !== undefined)
                        $($eleme[0])
                            .trigger('mouseenter')
                    break;

                /**
                 * Notificación de movimiento del puntero del ratón
                 */
                case 'mousemove':
                    if (event.to !== Cobrowser.clientId) return;

                    let diffTop = -2,
                        diffLeft = 0

                    // console.log(event.type + " => " + event.x + ", " + event.y)

                    $pointer
                        .css({
                            visibility: 'visible',
                            position: "absolute",
                            // marginLeft: (event.x / 2) + diffLeft,
                            // marginTop: (event.y / 2) + diffTop,
                            top: event.y + diffTop,
                            left: event.x + diffLeft
                        })

                    break;

                /**
                 * Notificación de colocación del puntero del ratón fue de un elemento del DOM
                 */
                case 'mouseout':
                    break;

                /**
                 * Notifificación de clic del puntero del ratón sobre un elemento del DOM
                 */
                case 'click':
                    if (event.to != Cobrowser.clientId) return;

                    let selector = (event.element.id) ? `#${event.element.id}` : `[name=${event.element.name}]`

                    console.log(event.type + " => " + selector)

                    let $element = $($(selector, $screen.contents())[0])
                    if ($element === undefined || $element === null) {
                        console.log(`Element from DOM = ${selector} not found or not indexed!`)
                        return
                    }

                    let tagName = $element.prop('tagName') ? $element.prop('tagName').toLowerCase() : ""
                    let tagType = $element.prop('type') ? $element.prop('type').toLowerCase() : ""

                    if (event.to == Cobrowser.clientId)
                        switch (tagName) {
                            case 'input':
                                switch (tagType) {
                                    case 'checkbox':
                                    case 'option':
                                        $element
                                            .attr('checked', true)
                                        break;

                                    case 'submit':
                                        console.log($element)
                                        $element
                                            .closest('form')
                                            .trigger('submit')
                                }
                            case 'textarea':
                                $element.focus()
                                break;

                            default:
                                $element.trigger('click', [event])
                        }

                        let $circle = $("#circle")
                        $circle
                            .css({
                                opacity: `0.8`,
                                left: `${event.x}px`,
                                top: `${event.y}px`,
                                width: '0px',
                                height: '0px'
                            })
                            .animate({
                                opacity: `0`,
                                left: `${event.x - 50}px`,
                                top: `${event.y - 50}px`,
                                width: '100px',
                                height: '100px'
                            })

                    // Animations.click($element, 0, 0)
                    break;

                /**
                 * Notificación de desplazamiento vertical y horizontal
                 */
                case 'scroll':
                    if (event.to != Cobrowser.clientId) return;

                    console.log(event.type + " => " + event.left + ", " + event.top)

                    Cobrowser.ownMovement = true;
                    _window.scrollTo(event.left, event.top)

                    setTimeout(function() {
                        Cobrowser.ownMovement = false;
                    },300);
                    break;

                /**
                 * Notificación de tecleado
                 */
                case 'keyup':
                    if (event.to !== Cobrowser.clientId) return;

                    let $focused = $(_window.document.activeElement);

                    console.log(event.type + " => " + event.key + ", " + event.code)

                    let newValue = $focused.val()

                    switch (event.key) {
                        case 'Backspace':
                            newValue = newValue.substr(0, newValue.length - 1)
                            break;

                        case 'undefined':
                        case 'Delete':
                        case 'Enter':
                        case 'Meta':
                        case 'Escape':
                        case 'Tab':
                        case 'Shift':
                        case 'CapsLock':
                        case 'Alt':
                        case 'AltGraph':
                        case 'Control':
                        case 'ArrowLeft':
                        case 'ArrowRight':
                        case 'ArrowUp':
                        case 'ArrowDown':
                            console.log('Special character received, skipped...')
                            break;

                        default:
                            if (event.value) {
                                console.log(event.type, event.source, event.value)

                                let selector = `#${event.source}`,
                                    $element = $(selector, $screen.contents())

                                $element.val(event.value)

                                return
                            }

                            newValue = $focused.val() + event.key
                    }
                    $focused.val(newValue)
                    break;

                case Cobrowser.events.COBROWSE_SELECTION:
                    if (event.to !== Cobrowser.clientId) return;

                    console.log(`${event.type} => #${event.source}: ${event.start}, ${event.end}`)

                    let sel = `#${event.source}`
                    let $el = $($(sel, $screen.contents())[0])
                    if ($el === undefined || $el === null) {
                        console.log(`Element from DOM = ${sel} not found or not indexed!`)
                        return
                    }
                    console.log($el[0])
                    $el[0].setSelectionRange(event.start, event.end)
                    break;

                default:

            }

        }
    },
    detach: function () {
        this.remoteId = ""

        with (Cobrowser) {
            hideSensitiveInformation = false
            remoteId = ""
            socket = null
        }

        this.socket = null
    },
    startLoadingAnimation: function (message) {
        let $body = $('body')

        with ($body) {
            loadingModal({'text': message || "Iniciando Cobrowsing..."})
            // loadingModal('animation', 'chasingDots')
            loadingModal('backgroundColor', 'blue')
        }
    },
    stopLoadingAnimation: function (message) {
        let $body = $('body')

        with ($body) {
            loadingModal('text', message || "Culminando...")

            Util.delay(100)
                .then(() => {
                    loadingModal('hide')
                    Util.delay(1000)
                        .then(() => loadingModal('destroy'))
                })
        }
    },
    snapshot: function () {
        with (Cobrowser) {
            let width = screenSize().width,
                height = screenSize().height

            let screenshotEvent = {
                origin: {
                    screen: {
                        width: width,
                        height: height
                    }
                },
                role: role,
                type: 'requestscreenshot',
                from: clientId,
                to: remoteId
            }
            socket.send(JSON.stringify(screenshotEvent))
        }

    },
    resize: function (width, height) {
        let $frame = $('#screen[name=agent]')

        console.log($frame)

        $frame.width(width)
        $frame.height(height)
        // var percent = 0.9;
        //
        // with (frame) {
        //     style.width = 100.0 / percent + "%";
        //     style.height = 100.0 / percent + "%";
        //
        //     style.zoom = percent;
        //     style.webkitTransform = 'scale(' + percent + ')';
        //     style.webkitTransformOrigin = 'top left';
        //     style.MozTransform = 'scale(' + percent + ')';
        //     style.MozTransformOrigin = 'top left';
        //     style.oTransform = 'scale(' + percent + ')';
        //     style.oTransformOrigin = 'top left';
        // }

    }
}
