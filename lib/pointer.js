// Hook pointer lock state change events for different browsers
window.addEventListener('pointerlockchange', lockChangeAlert, false);
window.addEventListener('mozpointerlockchange', lockChangeAlert, false);

function lockChangeAlert() {
    // if (document.pointerLockElement === canvas ||
    //     document.mozPointerLockElement === canvas) {
        console.log('The pointer lock status is now locked');
        window.addEventListener("mousemove", updatePosition, false);
    // } else {
    //     console.log('The pointer lock status is now unlocked');
    //     document.removeEventListener("mousemove", updatePosition, false);
    // }
    //
    // updatePosition()
}

function updatePosition(e) {
    x += e.movementX;
    y += e.movementY;

    console.log("X position: " + x + ", Y position: " + y);
}