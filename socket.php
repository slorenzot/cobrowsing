<?php
require_once "lib/websockets.php";

$host = "192.168.0.189";
$port = 9000;

class Interaction
{
    public function save()
    {

    }
}

class Session
{
    public function load()
    {

    }
}

/*
 *  sessionID UUID
 *  status      new,waitingpermission,opened,closed
 *  location    https://dialapplet.com
 *  webagent    mozilla,firefox,etc
 *  resolucion  1366x768
 */

/**
 * Class Socket
 */
class Socket extends WebSocketServer
{
    protected function process($user, $data)
    {
        foreach ($this->users as $currentUser) {
            $this->send($currentUser, $data);
        }
    }

    protected function connected($socket)
    {
        echo 'user connected' . PHP_EOL;
    }

    protected function closed($user)
    {
        echo 'user connected' . PHP_EOL;
    }
}

$socket = new Socket($host, $port);

try {
    $socket->run();
} catch (Exception $e) {
    $socket->stdout($e->getMessage());
}
?>